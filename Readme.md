Timeee Project Management
========
This repository servers as bug tracker for Timeee Project Management Tool.If you find any bugs or you have some proposals or enhancements feel free to report them here. 

Please check for existing issues first which may overlap with your request. If so please consider contributing to the exisiting issue rather than start a new one. 

**Please describe your issues in a precise and short manner.
**

Visit [Timeee](http://timeee.de "Timeee") for further information.  Timeee is created by  [Pixel Pub](http://pixel-pub.de "Pixel Pub")

Thank you very much!

The staff of Pixel Pub